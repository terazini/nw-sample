
all: run

# note that you should have nw on the path or aliased to the 
# path to the node-webkit binary
run: 
	nw `pwd`/src

app.nw: app.zip
	mv $< $@

app.zip: src/*
	cd src && zip $@ $(subst src/,,$^)
	mv src/app.zip ./

run-osx-packaged: app.nw
	open -n -a node-webkit --args `pwd`/app.nw

.PHONY: run
